import {Component, OnInit, OnDestroy} from '@angular/core'
import {Subscription} from 'rxjs/Subscription'
import {TranslateService} from '@ngx-translate/core'
import {ProductsService} from '../_services/products.service'
import {Product} from '../_models/product.model'

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})


export class ProductsComponent implements OnInit {
  private products: Product[] = []
  private errMessage: any
  private pending: boolean
  private changeLangSubscription: Subscription
  public count: number = 12
  public page: number = 1
  public total: number

  constructor(private _productsService: ProductsService, private translate: TranslateService) {
    this.page = this._productsService.getPage()
    let lang = translate.currentLang
    this.changeLangSubscription = translate.onLangChange.subscribe(($event) => {
      if (lang !== $event.lang) {
        lang = null
        this.getProducts()
      }
    })
  }

  ngOnInit() {
    this.getProducts()
  }
  
  ngOnDestroy() {
    this.changeLangSubscription.unsubscribe()
  }

  onPageChanged(event: any): void {
    if (event.page === this.page)
      return
    this.page = event.page
    this.getProducts()
  }

  // Получение товаров с сервера
  getProducts(): void {
    this.pending = true
    this._productsService.setPage(this.page)
    this._productsService.getProducts({lang: this.translate.currentLang, offset: (this.page - 1) * this.count, count: this.count})
      .subscribe(
        (res) => {
          this.products = res.products
          this.total = res.count
          this.pending = false
        },
        (err) => {
          this.errMessage = err
          this.pending = false
        }
      )
  }
}
