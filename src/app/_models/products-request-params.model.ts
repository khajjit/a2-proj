export class ProductsRequestParams {
  lang: string;
  offset?: number;
  count?: number;
  idList?: number[];
  localize?: string[];
}