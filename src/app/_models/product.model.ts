export class Product {
  public id: number;
  public quantity: number;
  public name: string;
  public description: string;
  public image: string;
  public price: number;

  constructor(id: number, quantity: number, name: string, description: string, image: string, price: number) {
    this.id = id
    this.quantity = quantity
    this.name = name
    this.description = description
    this.image = image
    this.price = price
  }
}
