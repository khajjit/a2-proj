export class Popup {
  public id?: number;
  public type?: string = "info";
  public text: string;
  public dismissible?: boolean = true;
  public delay?: number = 3000;

  constructor(params: Popup) {
    Object.assign(this, params)
  }
}
