import {Product} from './product.model'
import {BehaviorSubject} from 'rxjs/BehaviorSubject'
import {Observable} from 'rxjs/Observable'
import {PopupService} from '../_services/popup.service'
import {Subscription} from 'rxjs/Subscription'

export class CartProduct {
  public product: Product
  private _quantitySource = new BehaviorSubject<number>(0)
  public quantity$ = this._quantitySource.asObservable()
  public quantitySubscription: Subscription
  public popupService: PopupService

  get quantity(): number {
    return this._quantitySource.getValue()
  }
  set quantity(_quantity: number) {
    _quantity = Number(_quantity)
    if (_quantity > this.product.quantity) {
      _quantity = this.product.quantity
      this.popupService.doShow({type: 'danger', text: 'popup.quantityNotEnough'})
    }
    if (isNaN(_quantity)) {
      _quantity = 0
      this.popupService.doShow({type: 'danger', text: 'popup.errorInputQuantity'})
    }
    this._quantitySource.next(_quantity)
  }

  constructor(product: Product, quantity: number = 1, popupService) {
    this.popupService = popupService
    this.product = product
    this.quantity = quantity
  }
}
