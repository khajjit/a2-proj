import {Component, OnInit} from '@angular/core'
import {FormGroup, FormBuilder, Validators} from '@angular/forms'
import {TranslateService} from '@ngx-translate/core'
import {ShoppingCartService} from '../_services/shopping-cart.service'
import {PopupService} from '../_services/popup.service'
import {UserCard} from '../_interfaces/user-card.interface'
import {ValueIsNumber} from '../_directives/value-is-number.directive'
import {IsCardNumber} from '../_directives/is-card-number.directive'

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {
  public userCard: UserCard = {name: '', number: null, cvc: null, month: null, year: null}
  public userForm: FormGroup

  constructor(private cartService: ShoppingCartService, private translate: TranslateService, private popupService: PopupService, private fb: FormBuilder) {
  }
  
  ngOnInit() {
    this.buildForm()
  }
  
  public buildForm(): void {
    this.userForm = this.fb.group({
      'name': [this.userCard.name, [
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(30),
        Validators.pattern('[A-Za-z ]*')
        ]
      ],
      'card': [this.userCard.number, [
        Validators.required,
        IsCardNumber()
        ]
      ],
      'cvc': [this.userCard.cvc, [
        Validators.required,
        Validators.pattern('[0-9]{3}')
        ]
      ],
      'month': [this.userCard.month, [
        Validators.required,
        Validators.pattern('[0-9]{2}')
        ]
      ],
      'year': [this.userCard.year, [
        Validators.required,
        Validators.pattern('[0-9]{4}')
        ]
      ]
    })
    this.userForm.valueChanges.subscribe(data => this.onValueChanged(data))
    this.onValueChanged()
  }
  
  public onValueChanged(data?: any): void {
    if (!this.userForm) { return }
    const form = this.userForm

    for (const field in this.formErrors) {
      this.formErrors[field] = ''
      const control = form.get(field)
      
      if (control && control.dirty && !control.valid) {
        let messages = ''
        this.translate.get('checkout.validation.' + String(field)).subscribe(
          (data: string) => messages = data
        )
        let errorCount = 0
        for (const key in control.errors) {
          this.formErrors[field] += (errorCount++ ? '. ' : '') + messages[key]
        }
      }
    }
  }
  
  formErrors = {
    'name': '',
    'card': '',
    'cvc': '',
    'month': '',
    'year': ''
  }

  public onSubmit(): void {
    this.userCard = this.userForm.value  // При успешной валидации сохраняем данные в объект
    this.popupService.doShow({type: 'info', text: 'popup.funcNotReady'})
  }
}
