import {Component, OnInit, ElementRef} from '@angular/core'
import {CartProduct} from '../_models/cart-product.model'
import {ShoppingCartService} from '../_services/shopping-cart.service'
import {PopupService} from '../_services/popup.service'

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.css']
})

export class ShoppingCartComponent implements OnInit {
  private cartProducts: CartProduct[]
  private totalPrice: number = 0

  constructor(private _cartService: ShoppingCartService, private _popupService: PopupService, private elementRef: ElementRef) {
  }

  ngOnInit() {
    this.initialize()
  }
  
  // Вернет false, если загрузка закончилась
  public isPending(): boolean {
    return this._cartService.pending
  }

  // Событие при пользовательском вводе числа товара
  public onQuantityChanged(cartProduct: CartProduct): void {
    const elemInput = this.elementRef.nativeElement.querySelector('#quantity' + cartProduct.product.id)
    elemInput.value = cartProduct.quantity
    this.totalPrice = this._cartService.getTotalPrice()
  }

  // Событие при удалении товара из корзины
  public onRemoveCartProduct(_cartProduct: CartProduct): void {
    this._cartService.removeCartProduct(_cartProduct)
    this.initialize()
  }

  // Обновление данных о выбранных товарах, итоговой цене
  public initialize(): void {
    this.cartProducts = this._cartService.getProducts()
    this.totalPrice = this._cartService.getTotalPrice()
  }
}
