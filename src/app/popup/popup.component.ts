import {Component, OnInit} from '@angular/core'
import {PopupService} from '../_services/popup.service'
import {Popup} from '../_models/popup.model'

@Component({
  selector: 'app-popup',
  templateUrl: './popup.component.html'
})

export class PopupComponent implements OnInit {
  public popupList: Popup[] = []

  constructor(private _popupService: PopupService) {
  }

  ngOnInit() {
    this.popupList = this._popupService.getPopupList()
  }

  // Удаление popup'а
  public close(popup: Popup): void {
    this._popupService.close(popup)
  }
}
