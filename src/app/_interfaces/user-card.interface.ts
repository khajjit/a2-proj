export interface UserCard {
  name: string;
  number: number;
  cvc: number;
  month: number;
  year: number;
}