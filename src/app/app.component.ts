import {Component, OnInit, OnDestroy} from '@angular/core'
import {Subscription} from 'rxjs/Subscription'
import {PopupService} from './_services/popup.service'
import {TranslateService} from '@ngx-translate/core'
import {CookieService} from 'angular2-cookie/services/cookies.service'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
  languageSubscription: Subscription;

  constructor(private _popupService: PopupService, private translate: TranslateService, private _cookieService: CookieService) {
    const cookieLang = _cookieService.get('lang')
    const browserLang = translate.getBrowserLang()
    translate.addLangs(['en', 'ru'])
    translate.setDefaultLang('en')
    
    if (cookieLang && translate.getLangs().find((elem) => cookieLang === elem))
      translate.use(cookieLang)
    else
      translate.use(browserLang.match(/en|ru/) ? browserLang : 'en')
    
    this.languageSubscription = translate.onLangChange.subscribe((event) => {
      this._cookieService.put('lang', event.lang)
    })
  }

  ngOnInit() {
    this._popupService.doShow({type: 'success', text: 'popup.appWorks'})
  }
  
  ngOnDestroy() {
    this.languageSubscription.unsubscribe()
  }
}
