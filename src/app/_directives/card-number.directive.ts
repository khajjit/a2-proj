import { Directive, HostListener, ElementRef, Input } from '@angular/core'

@Directive({
  selector: `[card-number]`
})
export class CardNumberDirective {

  constructor(private element: ElementRef) {
  }

  @Input('card-number') formElement

  @HostListener('input', ['$event'])
  processEntry() {
    let element = this.element.nativeElement
    let position = element.selectionStart
    let value = element.value

    if(!element.value) {
      return
    }

    let result = ''
    for(let i = 0; i < value.length; i++) {
      if(result.replace(/-/g, '').length >= 20) {
        break
      }
      if((result.length + 1) % 5 === 0) {
        result += '-'
        if(value[i] !== '-') {
          position += position > i ? 1 : 0
        }
        else {
          continue
        }
      }
      if(value[i].match(/[^\d]/g)) {
        position -= position > i ? 1 : 0
        continue
      }
      result += value[i]
    }

    if(element.value !== result) {
      element.value = result
      element.selectionEnd = position
      this.formElement.setValue(result)
    }

    return
  } 
}