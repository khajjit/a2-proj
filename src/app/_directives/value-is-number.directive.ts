import {Directive} from '@angular/core'
import {AbstractControl, ValidatorFn} from '@angular/forms'

export function ValueIsNumber(): ValidatorFn {
  return (control: AbstractControl): {[key: string]: any} => {
    const isValid = /^(0|[1-9]\d*)$/.test(control.value)
    if (isValid) 
      return null
    else 
      return { 'isNumber': {isValid} }
  };
}