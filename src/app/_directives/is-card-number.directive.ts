import {Directive} from '@angular/core'
import {AbstractControl, ValidatorFn} from '@angular/forms'

export function IsCardNumber(): ValidatorFn {
  return (control: AbstractControl): {[key: string]: any} => {
    if(!control.value) {
      return
    }
      
    let numberCard = control.value.replace(/[^\d]/g, '')
    
    if(numberCard.length < 16) {
      return {
        'lessThan16': true
      }
    }

    if(numberCard.length != 16 && numberCard.length != 20) {
      return {
        'invalidAmount': true
      }
    }

    return
  }
}
