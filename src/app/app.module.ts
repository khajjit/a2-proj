import {BrowserModule} from '@angular/platform-browser'
import {NgModule} from '@angular/core'
import {FormsModule, ReactiveFormsModule} from '@angular/forms'
import {HttpModule, Http} from '@angular/http'
import {RouterModule, Routes} from '@angular/router'
import {AlertModule} from 'ng2-bootstrap'
import {BsDropdownModule} from 'ngx-bootstrap/dropdown'
import {PaginationModule} from 'ngx-bootstrap/pagination'
import {TooltipModule} from 'ngx-bootstrap/tooltip'
import {TranslateModule, TranslateLoader} from '@ngx-translate/core'
import {TranslateHttpLoader} from '@ngx-translate/http-loader'
import {CookieService} from 'angular2-cookie/services/cookies.service'

import {APP_ROUTES} from './app.routes'
import {AppComponent} from './app.component'
import {HeaderComponent} from './header/header.component'
import {HomeComponent} from './home/home.component'
import {ProductsComponent} from './products/products.component'
import {AboutComponent} from './about/about.component'
import {ProductComponent} from './product/product.component'
import {ProductsService} from './_services/products.service'
import {ShoppingCartService} from './_services/shopping-cart.service'
import {ShoppingCartComponent} from './shopping-cart/shopping-cart.component'
import {PopupComponent} from './popup/popup.component'
import {PopupService} from './_services/popup.service'
import {CheckoutComponent} from './checkout/checkout.component'

import { CardNumberDirective } from './_directives/card-number.directive'

export function createTranslateLoader(http: Http) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json')
}

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    ProductsComponent,
    AboutComponent,
    ProductComponent,
    ShoppingCartComponent,
    PopupComponent,
    CheckoutComponent,
    CardNumberDirective
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    PaginationModule.forRoot(),
    BsDropdownModule.forRoot(),
    TooltipModule.forRoot(),
    RouterModule.forRoot(APP_ROUTES),
    AlertModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [Http]
      }
    })
  ],
  providers: [ShoppingCartService, ProductsService, PopupService, CookieService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
