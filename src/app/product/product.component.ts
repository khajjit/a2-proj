import {Component, Input} from '@angular/core'
import {Product} from '../_models/product.model'
import {ShoppingCartService} from '../_services/shopping-cart.service'
import {PopupService} from '../_services/popup.service'

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})

export class ProductComponent {
  @Input() product: Product

  constructor(private cartService: ShoppingCartService, private popupService: PopupService) {
  }

  // Добавление товара пользователем в корзину
  public addToCart(product: Product): void {
    if (product.quantity === 0) {
      this.popupService.doShow({type: 'danger', text: 'popup.noQuantity'})
      return
    }
    this.cartService.addProduct(this.product)
  }
}
