import {Injectable} from '@angular/core'
import {Popup} from '../_models/popup.model'

@Injectable()
export class PopupService {
  private popupList: Popup[] = []
  private lastId: number = 0

  constructor() {
  }

  // Отдача массива popup'ов на компонент
  public getPopupList(): Popup[] {
    return this.popupList
  }

  // Создает popup
  public doShow(_popup: Popup): void {
    _popup.id = this.lastId
    this.popupList.push(new Popup(_popup))
    this.lastId++
  }

  // Удаляет popup
  public close(_popup: Popup): void {
    const index = this.popupList.findIndex(elem => _popup.id === elem.id)
    this.popupList.splice(index, 1)
  }
}
