import {Injectable} from '@angular/core'
import {Subject} from 'rxjs/Subject'
import {Observable} from 'rxjs/Observable'
import {Subscription} from 'rxjs/Subscription'
import {Product} from '../_models/product.model'
import {CartProduct} from '../_models/cart-product.model'
import {ProductsService} from '../_services/products.service'
import {TranslateService} from '@ngx-translate/core'
import {PopupService} from './popup.service'

@Injectable()
export class ShoppingCartService {
  public cartProducts: CartProduct[] = []  // Корзина товаров, набранных пользователем
  private totalPrice: number = 0  // Общая цена набранных товаров
  private totalQuantity: number = 0  // Общее количество товаров
  public pending: boolean = false
  private productsQuantitySource = new Subject<number>()
  public changeProductsQuantity$ = this.productsQuantitySource.asObservable() // подписка на кол-во товаров
  private changeLangSubscription: Subscription

  constructor(private popupService: PopupService, private productsService: ProductsService, private translate: TranslateService) {
    this.changeLangSubscription = translate.onLangChange.subscribe((event) => {
      this.renewProducts(event.lang)
    });
    this.restoreDataFromLocalStorage()
  }

  // Отдача данных о общем количестве набранных товаров
  public getTotalQuantity(): number {
    return this.totalQuantity
  }

  // Отдача данных об общей цене набранных товаров
  public getTotalPrice(): number {
    return this.totalPrice
  }

  // Отдача данных о набранных товарах
  public getProducts(): CartProduct[] {
    return this.cartProducts
  }

  // Добавляет выбранный пользователем товар в корзину
  public addProduct(_product: Product): void {
    const _cartProduct: CartProduct = this.cartProducts
      .find(element => element.product.id === _product.id)
    if (_cartProduct)
      _cartProduct.quantity++
    else
      this.addNewCartProduct(_product)
    this.setTotalPrice()
  }

  // Добавление товара, выбранного впервые
  private addNewCartProduct(_product: Product, quantity = 1): void {
    const _cartProduct: CartProduct = new CartProduct(_product, quantity, this.popupService);
    this.cartProducts.push(_cartProduct)
    // Подписка срабатывает при любом изменении количества товара
    _cartProduct.quantitySubscription = _cartProduct.quantity$
      .subscribe(() => this.processCartChanges())
  }

  // Удаление товара из корзины
  public removeCartProduct(_product: CartProduct): void {
    _product.quantitySubscription.unsubscribe()
    this.cartProducts = this.cartProducts.filter(elem => elem.product.id !== _product.product.id)
    this.processCartChanges()
  }

  // смена языка у имени/описания товаров в корзине
  private renewProducts(lang): void {
    if (!this.cartProducts.length) {
      return
    }
    this.pending = true
    const idList = this.cartProducts.map((cartProduct) => cartProduct.product.id)
    const localize = ['id', 'name', 'description']
    this.productsService.getProducts({lang: lang, idList: idList, localize: localize}).subscribe(serverData => {
      this.cartProducts.forEach((cartProduct) => {
        let findProduct = serverData.products.find((serverProduct) => cartProduct.product.id === serverProduct.id)
        if (findProduct) {
          cartProduct.product.name = findProduct.name
          cartProduct.product.description = findProduct.description
        }
      })
      this.pending = false
      this.saveDataToLocalStorage()
    })
  }

  // Обновление данных - количество товаров, итоговая цена и сохранение в Local Storage
  private processCartChanges(): void {
    this.setTotalQuantity()
    this.productsQuantitySource.next(this.totalQuantity) // данные на HeaderComponent
    this.setTotalPrice()
    this.saveDataToLocalStorage()
  }

  // Получение общей суммы товаров
  private setTotalPrice(): void {
    this.totalPrice = this.cartProducts.reduce((acc, val) => {
      return acc + val.product.price * val.quantity
    }, 0)
  }

  // Пересчет количества товаров, взятых пользователем
  private setTotalQuantity(): void {
    this.totalQuantity = this.cartProducts.reduce((acc, val) => {
      return acc + val.quantity
    }, 0)
  }

  // Сохранение данных локально
  private saveDataToLocalStorage(): void {
    const saveData = this.cartProducts.map(name => ({product: name.product, quantity: name.quantity}))
    localStorage.setItem('CART_PRODUCTS_KEY', JSON.stringify(saveData))
  }

  // Восстановление содержимого
  private restoreDataFromLocalStorage(): void {
    const loadData = JSON.parse(localStorage.getItem('CART_PRODUCTS_KEY')) || []
    loadData.forEach(item => this.addNewCartProduct(item.product, item.quantity))
  }
}
