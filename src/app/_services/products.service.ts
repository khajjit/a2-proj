import {Injectable} from '@angular/core'
import {ActivatedRoute, Router} from '@angular/router'
import {Product} from '../_models/product.model'
import {ProductsRequestParams} from '../_models/products-request-params.model'
import {Observable} from 'rxjs/Observable'
import {Http, Response} from '@angular/http'
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/catch'

@Injectable()
export class ProductsService {
  private productsUrl: string = 'api/products?'
  private page: number = 1

  constructor(private http: Http, private route: ActivatedRoute, private router: Router) {
    this.route.queryParams.subscribe(params => 
                                     this.page = +params['page'] || this.page || 1)
  }
  
  public setPage(_page: number): void {
    this.page = _page
    this.router.navigate(['/products'], {queryParams: {page: _page}}) // Меняем значение в URL
  }

  public getPage(): number {
    return this.page
  }

  getProducts(request: ProductsRequestParams): Observable<any> {
    const langQuery: string = request.lang !== undefined ? 'lang=' + request.lang : ''
    const idListQuery: string = request.idList !== undefined ? '&idList=' + request.idList : ''
    const localizeQuery: string = request.localize !== undefined ? '&localize=' + request.localize : ''
    const offset: string = request.offset !== undefined ? '&offset=' + request.offset : ''
    const count: string = request.count !== undefined ? '&count=' + request.count : ''
    return this.http.get(this.productsUrl + langQuery + offset + count + idListQuery 
                                          + localizeQuery).map(this.extractData).catch(this.handleError)
  }

  private extractData(res: Response) {
    const body = res.json()
    return body.data || {}
  }

  private handleError(error: Response | any) {
    let errMsg: string
    if (error instanceof Response) {
      const body = error.json() || ''
      const err = body.error || JSON.stringify(body)
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`
    } else {
      errMsg = error.message ? error.message : error.toString()
    }
    console.error(errMsg)
    return Observable.throw(errMsg)
  }
}
