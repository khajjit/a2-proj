import {Component, OnDestroy} from '@angular/core'
import {ShoppingCartService} from '../_services/shopping-cart.service'
import {Subscription} from 'rxjs/Subscription'
import {TranslateService} from '@ngx-translate/core'

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {
  private productsQuantity: number = 0
  subscriptionOnProductsQuantity: Subscription

  constructor(private _ShoppingCartService: ShoppingCartService, private translate: TranslateService) {
    this.productsQuantity = _ShoppingCartService.getTotalQuantity()
    this.subscriptionOnProductsQuantity = _ShoppingCartService.changeProductsQuantity$
      .subscribe(quantity => this.productsQuantity = quantity)
  }
  
  private onChangeLanguage(lang): void {
    if (this.translate.currentLang !== lang)
      this.translate.use(lang)
  }

  ngOnDestroy() {
    this.subscriptionOnProductsQuantity.unsubscribe()
  }
}
