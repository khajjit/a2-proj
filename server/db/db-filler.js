const fs = require('fs');
const faker = require('faker');
const path = require('path')
const sqlite3 = require('sqlite3').verbose();
const DATABASE_PATH = path.resolve(__dirname, '..', 'database.db');

const languages = ['ru', 'en'];
const GOODS_COUNT = 250;
const myGoods = {
  en: require('./myProducts.en.json'),
  ru: require('./myProducts.ru.json')
};

function setupGoods(db) {
  db.serialize(() => {
    db.run('CREATE TABLE Goods (id INTEGER PRIMARY KEY, quantity INTEGER NOT NULL, name TEXT NOT NULL, description TEXT NOT NULL, price INTEGER NOT NULL, image TEXT NOT NULL, language TEXT NOT NULL)', (err) =>
      console.log(err || 'Goods table was created.')
    );

    const stmt = db.prepare('INSERT INTO Goods VALUES (?, ?, ?, ?, ?, ?, ?)');
    
    // Наполняем базу данных товарами с английским описанием
    faker.locale = 'en';
    const lengthMyEnGoods = myGoods.en.products.length;
    for (let i = 0; i < lengthMyEnGoods; i++) {
      stmt.run(i + 1, myGoods.en.products[i].quantity, myGoods.en.products[i].name, myGoods.en.products[i].description, myGoods.en.products[i].price, myGoods.en.products[i].image, 'en');
    }
    let startPoint = lengthMyEnGoods + 1;
    let endPoint = lengthMyEnGoods + GOODS_COUNT;
    for (let i = startPoint; i <= endPoint; i++) {
      stmt.run(i, Math.floor(Math.random() * 20), faker.commerce.productName(), faker.commerce.productAdjective(), faker.commerce.price(), faker.image.imageUrl(), 'en');
    }

    // Наполняем базу данных товарами с русским описанием
    faker.locale = 'ru';
    const lengthMyRuGoods = myGoods.ru.products.length;
    startPoint = endPoint + 1;
    endPoint = endPoint + lengthMyRuGoods;
    for (let i = startPoint; i <= endPoint; i++) {
      let index = i - startPoint;
      stmt.run(i, myGoods.ru.products[index].quantity, myGoods.ru.products[index].name, myGoods.ru.products[index].description, myGoods.ru.products[index].price, myGoods.ru.products[index].image, 'ru');
    }
    startPoint = endPoint + 1;
    endPoint = startPoint + GOODS_COUNT;
    for (let i = startPoint; i < endPoint; i++) {
      stmt.run(i, Math.floor(Math.random() * 20), faker.commerce.productName(), faker.commerce.productAdjective(), faker.commerce.price(), faker.image.imageUrl(), 'ru');
    }

    stmt.finalize(() => console.log('Goods table was populated.'));
  });
}

function removeDB(cb) {
  fs.unlink(DATABASE_PATH, function() {
    console.log('Database was deleted!');
    cb();
  });
}

function generateDB() {
  const db = new sqlite3.Database(DATABASE_PATH);
  setupGoods(db);
  db.close();
}

if (fs.existsSync(DATABASE_PATH)) {
  removeDB(generateDB);
} else {
  generateDB();
}
