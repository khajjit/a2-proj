const path = require('path');
const express = require('express');
const app = express();
const sqlite3 = require('sqlite3');
const port = 3000;

const db = new sqlite3.Database('server/database.db', sqlite3.OPEN_READWRITE, (err) => {
  if (err)
    throw ('No database.db found. Run "node db-filler.js"...')
});

function findGoods(req, res, lang, offset, count, idList = false, localize = false) {
  db.all('SELECT COUNT(id) FROM Goods WHERE language = ?', lang, (error, countGoods) => {
    if (error) {
      throw error
    }
    if (idList) {
      idList = idList.split(',');
      db.all('SELECT * FROM Goods', (error, result) => {
        if (error) {
          throw error
        }
        result = result.filter(elem => idList.indexOf(String(elem.id)) !== -1);
        res.send({
          data: {products: result, count: countGoods[0]['COUNT(id)']}
        })
      })
    } else {
      db.all('SELECT * FROM Goods WHERE language = ? LIMIT ? OFFSET ?', [lang, count, offset], (error, result) => {
        res.send({
          data: {products: result, count: countGoods[0]['COUNT(id)']}
        })
      })
    }
  })
}

app.get('/api/test', function (req, res) {
  res.send('API server works!');
});

app.get('/api/products', function (req, res) {
  let delay = process.env.MOCK_TIMEOUT || 0;
  let lang = req.query.lang;
  let idList = req.query.idList;
  let localize = req.query.localize;
  let offset = req.query.offset;
  let count = req.query.count;
  setTimeout(function () {
    findGoods(req, res, lang, offset, count, idList, localize);
  }, delay);
});

if(process.env.NODE_ENV === 'production') {
  app.use(express.static(path.join(__dirname, '..', 'dist')));

  app.get('/*', function(req, res) {
    res.sendFile(path.join(__dirname, '../dist/index.html'))
  });
}

app.listen(process.env.PORT || port, function() {
  let listenPort = process.env.PORT || port;
  console.log('Hello, console! Listening on port ' + listenPort + '...');
});
